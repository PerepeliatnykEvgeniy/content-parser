create table if not exists user_role(
    id                    SERIAL PRIMARY KEY,
    role                  VARCHAR(1024) NOT NULL,
    user_id               BIGINT NOT NULL,
    FOREIGN KEY (user_id) REFERENCES users(id)
);