create TABLE IF NOT EXISTS song_statistic
(
id serial primary key,
"date" date,
"timestamp" bigint,
content_url varchar(256),
"content" text,
amount_same_words bigint,
total_words_amount bigint,
most_popular_words varchar(50),
amount_unique_words bigint
);