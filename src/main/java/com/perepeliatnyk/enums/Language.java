package com.perepeliatnyk.enums;

public enum Language {

    ENGLISH, RUSSIAN, OTHER
}
