package com.perepeliatnyk.services;

import com.perepeliatnyk.model.SongInfo;
import org.jsoup.nodes.Document;
import java.util.List;


public interface PageParser {

    List<SongInfo> parsWebPage(Document page);

    String getTargetUrl();

}
