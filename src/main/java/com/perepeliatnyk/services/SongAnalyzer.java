package com.perepeliatnyk.services;

import com.perepeliatnyk.model.SongInfo;
import com.perepeliatnyk.dao.SongStatisticEntity;
import com.perepeliatnyk.model.Word;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class SongAnalyzer {
    private List<WordFunction> wordFunctionList;

    public SongAnalyzer(List<WordFunction> wordFunctionList) {
        this.wordFunctionList = wordFunctionList;
    }

    public List<SongStatisticEntity> analyzeSong (List<SongInfo> songInfo){
        List<SongStatisticEntity> songStatisticEntities = new ArrayList<>();
        for (SongInfo info : songInfo) {
            SongStatisticEntity songStatisticEntity = new SongStatisticEntity();
            Map<String, Word> wordMap = getWords(info);
            for (WordFunction wordFunction : wordFunctionList) {
                wordFunction.apply(wordMap,songStatisticEntity);
            }
            songStatisticEntity.setDate(Calendar.getInstance().getTime());
            songStatisticEntity.setTimestamp(System.currentTimeMillis());
            songStatisticEntity.setContentUrl(info.getUrl());
            songStatisticEntity.setContent(String.join("\n",info.getLines()));
            songStatisticEntities.add(songStatisticEntity);
        }

        return songStatisticEntities;
    }

    private Map<String, Word> getWords(SongInfo songInfo) {
        Map<String, Word> stringWordMap = new HashMap<>();
        for (String line : songInfo.getLines()) {
            line = line.toLowerCase();
            for (String word : line.split("[!?:\\-.,\\s]+")) {
                if(word.equals("")) continue;
                if(stringWordMap.get(word)==null) stringWordMap.put(word,new Word(word));
                else stringWordMap.get(word).count();
            }

        }
        return stringWordMap;
    }
}
