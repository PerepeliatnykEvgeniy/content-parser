package com.perepeliatnyk.services;

import com.perepeliatnyk.dao.SongStatisticEntity;
import com.perepeliatnyk.model.Word;

import java.util.Map;

public class AmountPopularWord implements WordFunction {


    @Override
    public SongStatisticEntity apply(Map<String, Word> stringWordMap, SongStatisticEntity songStatisticEntity) {
        Word word = stringWordMap.values()
                                 .stream()
                                 .max(Word::compareTo)
                                 .get();
        songStatisticEntity.setMostPopularWords(word.getWord());
        return songStatisticEntity;
    }
}