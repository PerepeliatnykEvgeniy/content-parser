package com.perepeliatnyk.services;

import com.perepeliatnyk.dao.SongStatisticEntity;
import com.perepeliatnyk.model.Word;
import org.springframework.stereotype.Component;
import java.util.Map;
import static com.perepeliatnyk.enums.Language.*;

@Component
public class LanguageDeterminer implements WordFunction {

    private final WordsScanner wordsScanner;

    public LanguageDeterminer(WordsScanner wordsScanner) {
        this.wordsScanner = wordsScanner;
    }

    @Override
    public SongStatisticEntity apply(Map<String, Word> stringWordMap, SongStatisticEntity songStatisticEntity) {
        if (wordsScanner.hasEnglishWords(stringWordMap)) {
            songStatisticEntity.setLanguage(ENGLISH);
        } else if (wordsScanner.hasRussianWords(stringWordMap)) {
            songStatisticEntity.setLanguage(RUSSIAN);
        } else {
            songStatisticEntity.setLanguage(OTHER);
        }
        return songStatisticEntity;
    }
}
