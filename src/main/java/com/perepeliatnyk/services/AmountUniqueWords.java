package com.perepeliatnyk.services;

import com.perepeliatnyk.dao.SongStatisticEntity;
import com.perepeliatnyk.model.Word;

import java.util.Map;

public class AmountUniqueWords implements WordFunction{

    @Override
    public SongStatisticEntity apply(Map<String, Word> stringWordMap, SongStatisticEntity songStatisticEntity) {
        Integer number = (int) stringWordMap.values()
                                            .stream()
                                            .filter(word -> word.getAmount() == 1)
                                            .count();
        songStatisticEntity.setAmountUniqueWords(number);
        return songStatisticEntity;
    }
}
