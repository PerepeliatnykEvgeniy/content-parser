package com.perepeliatnyk.services;

import com.perepeliatnyk.enums.UserRole;

public class Role {
    private UserRole role;

    public UserRole getRole() {
        return role;
    }

    public void setRole(UserRole role) {
        this.role = role;
    }
}