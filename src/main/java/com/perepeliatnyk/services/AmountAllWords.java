package com.perepeliatnyk.services;

import com.perepeliatnyk.dao.SongStatisticEntity;
import com.perepeliatnyk.exception.TextAnalyzerServiceException;
import com.perepeliatnyk.model.Word;

import java.util.Map;

public class AmountAllWords implements WordFunction {
    @Override
    public SongStatisticEntity apply(Map<String, Word> stringWordMap,
                                     SongStatisticEntity songStatisticEntity) {
        if (songStatisticEntity == null) {
            throw new TextAnalyzerServiceException("SongStatisticEntity can't be null.");
        }
        if (stringWordMap == null) {
            throw new TextAnalyzerServiceException("Words map can't be null.");
        }
        Integer number = stringWordMap.values()
                                      .stream()
                                      .mapToInt(Word::getAmount)
                                      .sum();
        songStatisticEntity.setTotalWordsAmount(number);
        return songStatisticEntity;
    }
}
