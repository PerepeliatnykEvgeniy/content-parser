package com.perepeliatnyk.services;

import com.perepeliatnyk.dao.SongStatisticEntity;
import com.perepeliatnyk.model.Word;

import java.util.Map;

public interface WordFunction {
    SongStatisticEntity apply(Map<String, Word>stringWordMap,SongStatisticEntity songStatisticEntity);
}
