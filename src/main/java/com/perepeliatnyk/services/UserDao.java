package com.perepeliatnyk.services;

import com.perepeliatnyk.dao.UserEntity;
import org.springframework.data.repository.CrudRepository;

public interface UserDao extends CrudRepository<UserEntity,Long> {

    UserEntity findOneByEmail(String email);

}
