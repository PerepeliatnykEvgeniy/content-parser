package com.perepeliatnyk.services;


import com.perepeliatnyk.model.Word;
import org.springframework.stereotype.Component;
import java.util.Map;

@Component
public class WordsScanner {

    private final String[] englishKeyWords = {"the", "of", "and", "a", "to"};
    private final String[] russianKeyWords = {"и", "в", "не", "на", "я"};

    public boolean hasEnglishWords(Map<String, Word> stringWordMap) {
        return lookForWords(stringWordMap, englishKeyWords);
    }

    public boolean hasRussianWords(Map<String, Word> stringWordMap) {
        return lookForWords(stringWordMap, russianKeyWords);
    }

    private boolean lookForWords(Map<String, Word> stringWordMap, String[] keyWords) {

        int counter = 0;
        for (String word : stringWordMap.keySet()) {
            for (String keyWord : keyWords) {
                if (word.equalsIgnoreCase(keyWord)) {
                    counter = counter + stringWordMap.get(word)
                                                     .getAmount();
                }
            }
        }
        return counter > 10;
    }
}
